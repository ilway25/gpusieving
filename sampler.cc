#include "sampler.h"
#include <unistd.h>

#define PI 3.141592653589793238462643383

// Assume b is BKZ-reduced
int main(int argc, char **argv)
{
   int option;
   while ((option = getopt(argc, argv, "r:")) != -1)
   {
      switch (option)
      {
      case 'r':
         int seed = atoi(optarg);
         if (seed == -1) seed = time(NULL);
         cerr << "Initializing with seed " << seed << endl;
         srand(seed);
         break;
      }
   }

   int in_gp = 900;
   int N = 0;

   mat_ZZ B;
   cin >> B;

   N = B.NumRows();

   KleinSampler sampler;
   sampler.Init(B, in_gp);

   while (true)
   {
      ListPoint *p = sampler.Sample();

      for (int j = 0; j < N; ++j)
         cout << p->v[j] << ' ';
      cout << endl;
      cout << p->norm;
      cout << endl;
      DeleteListPoint(p);
   }
}

long KleinSampler::SampleZ(double c, double s_square, int curr_dimension)
{
   int e, a;
   float f, z;
   f = 0;
   z = 1;
   do
   {
      e = int(round((lookup_boundary_ - 1) * (float)((float)(rand()) / ((float)(RAND_MAX)))));
      f = *(dg_lookup + e + curr_dimension * lookup_boundary_);
      z = (float)(rand()) / (float)(RAND_MAX);
   }
   while (z > f);

   a = 2 * (rand() % 2) - 1;

   return ((int)round(c + a * e));

}

void KleinSampler::Init(const mat_ZZ &B, float in_gp)
{
   n_ = B.NumRows();
   m_ = B.NumCols();
   mat_RR mu;
   in_gp_ = in_gp;
   vec_RR Bstar_square;
   ComputeGS(B, mu, Bstar_square);
   MatInt64FromMatZZ(B_, B);
   MatDoubleFromMatRR(mu_, mu);
   coef_.SetLength(n_);
   s_prime_square_.SetLength(n_);
   long max_star_sqr_norm = 0;
   long min_star_sqr_norm = to_long(Bstar_square[0]);
   for (int i = 0; i < n_; ++i)
   {
      max_star_sqr_norm = max(max_star_sqr_norm, to_long(Bstar_square[i]));
      min_star_sqr_norm = min(min_star_sqr_norm, to_long(Bstar_square[i]));
   }
   t_ = log(n_);
   double s_square = max_star_sqr_norm * log(n_);
   for (int i = 0; i < n_; ++i)
      s_prime_square_[i] = s_square / to_double(Bstar_square[i]);
   //Allocating for the lookup table
   //A boundary of ceil(3 * min_norm) is hard-coded
   int lookup_boundary;
   // fprintf(stderr, "in_gp_: %f\n", in_gp_);
   // fprintf(stderr, "min_star_sqr_norm: %f\n", min_star_sqr_norm);
   lookup_boundary = int(ceil(3 * in_gp_ / (float)(sqrt(min_star_sqr_norm))));
   lookup_boundary = (lookup_boundary == 0) ? 1 : lookup_boundary;
   dg_lookup = (float *)malloc(sizeof(float) * lookup_boundary * n_);
   lookup_boundary_ = lookup_boundary;
   //Now filling this is
   float d_acc, curr_s;
   for (int temp_i = 0; temp_i < n_; temp_i++)
   {
      d_acc = 0;
      curr_s = in_gp_ / sqrt(to_double(Bstar_square[temp_i]));
      for (int temp_j = 0; temp_j < lookup_boundary; temp_j++)
         d_acc += (float)(exp(-((pow(temp_j, 2) * PI) / (pow(curr_s, 2)))));
      for (int temp_j = 0; temp_j < lookup_boundary; temp_j++)
         *(dg_lookup + temp_i * lookup_boundary + temp_j) = (float)(exp((-pow(temp_j, 2) * PI) / (pow(curr_s, 2))) / d_acc);
   }
}

ListPoint *KleinSampler::Sample()
{
   ListPoint *lp = NewListPoint(m_);
   for (int i = 0; i < n_; ++i)
      coef_[i] = 0;
   for (int i = n_ - 1; i >= 0; --i)
   {
      coef_[i] = SampleZ(coef_[i], s_prime_square_[i] / in_gp_, i);
      for (int j = 0; j < i; j++)
         coef_[j] -= (coef_[i] * mu_[i][j]);
   }
   lp->norm = 0;
   for (int i = 0; i < m_; i++)
   {
      for (int j = 0; j < n_; j++)
         lp->v[i] += coef_[j] * B_[j][i];
      lp->norm += lp->v[i] * lp->v[i];
   }
   return lp;
}
