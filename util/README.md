Benchmark
=========

This benchmark aims to provide a rough timing estimate for different strategies that can be used to implement GaussSieve. This also demostrates how such a simple kernel can be accelerated.

Simplification
--------------
Let P and Q be lists containing vectors. In GaussSieve, `reduce(P, Q)` reduces the vectors in P with those in Q.

```python
def reduce(P, Q):
    for p in P:
        for q in Q:
            p = reduce p with q
```

Here, we benchmark a much simplified version: each vector in P is added with every vector in Q.

```python
def foo(P, Q):
    for p in P:
        for q in Q:
            p = p + q    # replace with vector addition
```

The behavior of these two functions are similar, except the previous suffers more from register pressure.

Strategies
----------

### Deafult

This is the naïve implementation (by those who claim they write CUDA). Each thread deals with one vector in P. 

Time | Thread 0 | Thread 1
---- | -------- | --------
0    | P[0][0]  | P[1][0]
1    | P[0][1]  | P[1][1]

### Rake

Imangine a rake with n branches: there are n consequtive threads cooperatively reading one vector in P. This can spread the register usage for each vector over several threads, reducing each thread's register pressure by 1/n.

Rake (width 4):

Time | Thread 0 to 3 | Thread 4 to 7
---- | ------------- | -------------
0    | P[0][0..3]    | P[1][0..3]
1    | P[0][4..7]    | P[1][4..7]

Rake 1 is simply Default.

### Load/Store

CUDA provides intrinsic types `float2` and `float4` that issue 64-bit and 128-bit global memory load and store instructions respectively.

`float2` generates `LD.E.64` and `ST.E.64`:

Time | Thread 0   | Thread 1
---- | ---------- | ----------
0    | P[0][0..1] | P[1][0..1]
1    | P[0][2..3] | P[1][2..3]

`float4` generates `LD.E.128` and `ST.E.128`:

Time | Thread 0   | Thread 1
---- | ---------- | ----------
0    | P[0][0..3] | P[1][0..3]
1    | P[0][4..7] | P[1][4..7]

### Shared Memory

Loading the same vector from Q in the global memory causes replays. Threads in a block can cooperate to load several vectors from Q for later use.

### ILP

Instruction level parallelism (ILP) deals with two or more different vectors simultaneous to hide instruction latency.

ILP 2:

Time | Thread 0          | Thread 1
---- | ----------------- | ------------------
0    | P[0][0], P[n][0]  | P[1][0], P[n+1][0]
1    | P[0][1], P[n][1]  | P[1][1], P[n+1][1]

Results
-------

Combinations of strategies yield different timings. The following tests were evolved in order and run on GTX Titan with |P| = 32768 and |Q| = 16384. These vectors are of dimension 80. They are stored in the memory as a continous block, without padding between consecutive vectors.

The first one is the reference implementation with 15 lines of code; the last, and the fastest one, uses all strategies and requires 40 lines of _nasty_ code.

No | Rake | Load/Store | Shared Memory | ILP | Register usage | Time
-: | :--: | :--------: | ------------: | --: | -------------: | ---:
1  |      |            |               |     | 93             | 387 
2  | 8    |            |               |     | 45             | 286 
3  |      |            | 1             |     | 96             | 86.5
4  |      | float2     |               |     | 98             | 343
5  |      | float4     |               |     | 127            | 264
6  |      | float2     | 3             |     | 100            | 81.7
7  |      | float4     | 24            |     | 96             | 66.8
8  | 8    |            | 1             |     | 42             | 210
9  | 8    | float2     |               |     | 45             | 205
10 | 4    | float4     |               |     | 54             | 192
11 | 4    | float4     | 75            |     | 47             | 63
12 | 4    | float4     | 36            | 4   | 110            | 37.3 

Choice of parameter:

- Block dimension: 64, 128, 256
- Rake: 1, 2, 4, 8
- Load/Store: float, float2, float4
- Shared memory: Depends on the block dimension. Usually can we have up to 6 choices.
- ILP: 1, 2, 4, 8

We will want to choose the fastest set of parameter from these 864 different combinations.

Future Work
-----------

There are other techniques. We may pad the 80 dimension vectors to align properly (pitch in CUDA). Interleaving several vectors in the memory might also boost the memory bandwidth. However these all complicate the code a lot more.