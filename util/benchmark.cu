#include <iostream>
#include <cassert>

#include <cub/cub.cuh>

using namespace std;

#undef CubDebug
#define CubDebug CubDebugExit

#define N       80
#define SIZE_U  32768
#define SIZE_V  16384

// NOTE: Register usage for these kernels is only HALF of gsieve's
//       because v is read and used immediately, not saved for future use

/*
Behavior of kernel:

 Global Memory Store Efficiency          100%
 Global Store Transactions Per Request   1.000000
 Global Store Transactions               81920 (* 128 = 32768 * 80 * 4)

 Global Memory Load Efficiency           100%
 Global Load Transactions Per Request    1.599963     = 8 transactions / 5 (warps, or requests)
 Global Load Transactions                2147565568

 L2 Read Transactions                    5369087113

 --- Global Load for V ---

 5 warps issues 8 transactions (128 byte x 2, 64 byte x 4)

 2147565568 - 81920 = 2147483648 transactions
 2147483648 / 1.6 = 1342177280 requests
 1342177280 * 128 = 32768 * 16384 * 80 * 4

 --- L2 ---
 5369087113 * 32 = 32768 * 16384 * 80 * 4
*/

__global__
void kernel(float *out, float *u, float *v)
{
   for (int i = blockIdx.x * blockDim.x + threadIdx.x;
        i < N * SIZE_U;
        i += gridDim.x * blockDim.x)
   {
      float r = u[i];

      for (int j = i % N; j < N * SIZE_V; j += N)
         r += v[j];

      out[i] = r;
   }
}

/*
Behavior of normal:

       Global Memory Store Efficiency      12.50%
Global Store Transactions Per Request   32.000000   -- Apparently stores in a warp will be 32 1-segment(32 byte) transactions
            Global Store Transactions     2621440

       Global Memory Load Efficiency     399.24%
Global Load Transactions Per Request    1.001892
            Global Load Transactions  1344798720

                L2 Read Transactions  1344817717

-- Store --

2621440 / 32 = 81920
81920 = 32768 / 32 * 80

-- Load for V --

1344798720 - 2621440 = 1342177280
1342177280 = (32768 / 32) * 16384 * 80 (each word in V takes one 1-segment transaction per warp (per 32 v's))

Note: only 4 bytes in each 1-segment transaction are used => 12.5%

-- L2 Read --

As in global load

*/

__global__
void normal(float *out, float *u, float *v)
{
   for (int n = blockIdx.x * blockDim.x + threadIdx.x;
            n < SIZE_U;
            n += gridDim.x * blockDim.x)
   {
      float r[N];
      for (int i = 0; i < N; ++i)
         r[i] = u[n * N + i];

      for (int j = 0; j < SIZE_V; ++j)
         for (int k = 0; k < N; ++k)
            r[k] += v[j * N + k];

      for (int i = 0; i < N; ++i)
         out[n * N + i] = r[i];
   }
}

#define idx    (blockIdx.x * blockDim.x + threadIdx.x)
#define inst   (idx / W)
#define subidx (idx % W)

/*
normalRake1<8>

       Global Memory Store Efficiency     100.00%
Global Store Transactions Per Request    4.000000  -- 4 1-segment stores
            Global Store Transactions      327680

       Global Memory Load Efficiency     399.93%
Global Load Transactions Per Request    1.000183
            Global Load Transactions  1342504960   -- 1-segment transactions

  L2 Read Transactions  1342519619

-- Global Load --

1342504960 - 327680 = 1342177280
1342177280 = (32768 / (32 / 8)) * (16384 * 10)

*/

// W : RakeWidth
template <const int W>
__global__
void normalRake1(float *out, float *u, float *v)
{
   for (int n = inst;
            n < SIZE_U;
            n += gridDim.x * blockDim.x / W)
   {
      float r[N / W];
      for (int i = 0; i < N / W; ++i)
         r[i] = u[n * N + i * W + subidx];

      for (int j = 0; j < SIZE_V; ++j)
         for (int k = 0; k < N / W; ++k)
            r[k] += v[j * N + k * W + subidx];

      for (int i = 0; i < N / W; ++i)
         out[n * N + i * W + subidx] = r[i];
   }
}

/*
normalRake2<8>

       Global Memory Store Efficiency     100.00%     100.00%     100.00%
Global Store Transactions Per Request    4.000000    4.000000    4.000000
            Global Store Transactions      327680      327680      327680

        Global Memory Load Efficiency     399.93%     399.93%     399.93%
 Global Load Transactions Per Request    1.000183    1.000183    1.000183
             Global Load Transactions  1342504960  1342504960  1342504960

                 L2 Read Transactions  1342524733  1342524733  1342524733

Same as normalRake1<8>
*/

template <const int W>
__global__
void normalRake2(float *out, float *u, float *v)
{
   for (int n = inst;
            n < SIZE_U;
            n += gridDim.x * blockDim.x / W)
   {
      float r[N / W];
      float *ptr = &u[n * N + subidx];

      for (int i = 0; i < N / W; ++i)
      {
         r[i] = *ptr;
         ptr += W;
      }

      for (int j = 0; j < SIZE_V; ++j)
      {
         ptr = &v[j * N + subidx];
         for (int k = 0; k < N / W; ++k)
         {
            r[k] += *ptr;
            ptr += W;
         }
      }

      ptr = &out[n * N + subidx];
      for (int i = 0; i < N / W; ++i)
      {
         *ptr = r[i];
         ptr += W;
      }
   }
}

/*
Behavior of normal pitch:

       Global Memory Store Efficiency      12.50%
Global Store Transactions Per Request   32.000000
            Global Store Transactions     2621440

       Global Memory Load Efficiency     399.24%
Global Load Transactions Per Request    1.001892
            Global Load Transactions  1344798720

                L2 Read Transactions  1344823791

Same as without pitch
*/

#if 0
__global__
void normalPitch(float *out, float *u, float *v, size_t pitch)
{
   for (int n = blockIdx.x * blockDim.x + threadIdx.x;
            n < SIZE_U;
            n += gridDim.x * blockDim.x)
   {
      float r[N];
      for (int i = 0; i < N; ++i)
         r[i] = u[n * pitch + i];

      for (int j = 0; j < SIZE_V; ++j)
         for (int k = 0; k < N; ++k)
            r[k] += v[j * pitch + k];

      for (int i = 0; i < N; ++i)
         out[n * pitch + i] = r[i];
   }
}

/*
Behavior of normal rake pitch:

       Global Memory Store Efficiency     100.00%
Global Store Transactions Per Request    4.000000
            Global Store Transactions      327680

       Global Memory Load Efficiency     399.93%
Global Load Transactions Per Request    1.000183
            Global Load Transactions  1342504960

            L2 Read Transactions  1342521371

Same as without pitch.
*/

template <const int W>
__global__
void normalRakePitch(float *out, float *u, float *v, size_t pitch)
{
   for (int n = inst;
            n < SIZE_U;
            n += gridDim.x * blockDim.x / W)
   {
      float r[N / W];
      for (int i = 0; i < N / W; ++i)
         r[i] = u[n * pitch + i * W + subidx];

      for (int j = 0; j < SIZE_V; ++j)
         for (int k = 0; k < N / W; ++k)
            r[k] += v[j * pitch + k * W + subidx];

      for (int i = 0; i < N / W; ++i)
         out[n * pitch + i * W + subidx] = r[i];
   }
}
#endif

/*
       Global Memory Store Efficiency      12.50%
Global Store Transactions Per Request   32.000000
            Global Store Transactions     2621440

        Global Memory Load Efficiency      94.85%
 Global Load Transactions Per Request    1.531695
             Global Load Transactions    19398656

                 L2 Read Transactions    44569275

Shared Memory Store Transactions Per Req    1.000000
               Shared Store Transactions    12582912

Shared Memory Load Transactions Per Requ    2.000000  -- Why?
                Shared Load Transactions   671088640

19398656 - 2621440 = 16777216
16777216 = 16384 * 4

-- Shared Memory Stores --

12582912 = 16384 * 3 * (32768 / 128)

-- Shared Memory Loads --

671088640 = (32768 / 128) * 16384 * 80 * 2 n

*/

__global__
void normalShared(float *out, float *u, float *v)
{
   const int NumPreLoad = 1;
   __shared__ float data[NumPreLoad][N];

   for (int n = blockIdx.x * blockDim.x + threadIdx.x;
            n < (SIZE_U + blockDim.x - 1) / blockDim.x * blockDim.x;
            n += gridDim.x * blockDim.x)
   {
      float r[N];

      if (n < SIZE_U)  // Can be removed if |U| is a multiple blockDim
         for (int i = 0; i < N; ++i)
            r[i] = u[n * N + i];

      for (int j = 0; j < SIZE_V; j += NumPreLoad)
      {
         for (int k = 0; k < NumPreLoad && j + k < SIZE_V; ++k)
            for (int index = threadIdx.x; index < N; index += blockDim.x)
               data[k][index] = v[(j + k) * N + index];

         __syncthreads();

         for (int k = 0; k < NumPreLoad && j + k < SIZE_V; ++k)
            for (int l = 0; l < N; ++l)
               r[l] += data[k][l];  // Profiler says there is one replay. Why??
                                    // It seems LDS.128 will cause one replay

         __syncthreads();
      }

      if (n < SIZE_U) // Remove if |U| is a multiple blockDim
         for (int i = 0; i < N; ++i)
            out[n * N + i] = r[i];
   }
}

/*
normalFloat 2

          Global Memory Store Efficiency      16.95%      16.95%      16.95%
   Global Store Transactions Per Request   32.000000   32.000000   32.000000
               Global Store Transactions     1933312     1933312     1933312

           Global Memory Load Efficiency     798.49%     798.49%     798.49%
    Global Load Transactions Per Request    1.001892    1.001892    1.001892
                Global Load Transactions   672399360   672399360   672399360

                    L2 Read Transactions   672416994   672416994   672416994

-- Store --

1933312 / 32 = 60416

*/

__global__
void normalFloat2(float *out, float *u, float *v)
{
   for (int n = blockIdx.x * blockDim.x + threadIdx.x;
            n < SIZE_U;
            n += gridDim.x * blockDim.x)
   {
      float2 r[N / 2];
      for (int i = 0; i < N / 2; ++i)
         r[i] = ((float2*)u)[n * N / 2 + i];


      for (int j = 0; j < SIZE_V; ++j)
         for (int k = 0; k < N / 2; ++k)
         {
            float2 data = ((float2*)v)[j * N / 2 + k];
            r[k].x += data.x;
            r[k].y += data.y;
         }

      // Why don't always issue ST.E.64, but interleave with ST.E ?
      for (int i = 0; i < N / 2; ++i)
         ((float2*)out)[n * N / 2 + i] = r[i];
   }
}

/*
normalFloat 4

          Global Memory Store Efficiency      13.16%      13.16%      13.16%
   Global Store Transactions Per Request   32.000000   32.000000   32.000000
               Global Store Transactions     2490368     2490368     2490368

           Global Memory Load Efficiency     799.27%     799.27%     799.27%
    Global Load Transactions Per Request    2.001831    2.001831    2.001831
                Global Load Transactions   671744000   671744000   671744000

                    L2 Read Transactions   671758067   671758067   671758067
*/

__global__
void normalFloat4(float *out, float *u, float *v)
{
   for (int n = blockIdx.x * blockDim.x + threadIdx.x;
            n < SIZE_U;
            n += gridDim.x * blockDim.x)
   {
      float4 r[N / 4];
      for (int i = 0; i < N / 4; ++i)
         r[i] = ((float4*)u)[n * N / 4 + i];

      for (int j = 0; j < SIZE_V; ++j)
         for (int k = 0; k < N / 4; ++k)
         {
            float4 data = ((float4*)v)[j * N / 4 + k];
            r[k].x += data.x;
            r[k].y += data.y;
            r[k].z += data.z;
            r[k].w += data.w;
         }

      for (int i = 0; i < N / 4; ++i)
         ((float4*)out)[n * N / 4 + i] = r[i];
   }
}

// Works for all parameters, but requires 2 | N
__global__
void normalFloat2Shared(float * out, float *u, float *v)
{
   const int NumPreLoad = 3;
   __shared__ float2 data[NumPreLoad][N / 2];

   for (int n = blockIdx.x * blockDim.x + threadIdx.x;
            n < (SIZE_U + blockDim.x - 1) / blockDim.x * blockDim.x;
            n += gridDim.x * blockDim.x)
   {
      float2 r[N / 2];

      if (n < SIZE_U)
         for (int i = 0; i < N / 2; ++i)
            r[i] = ((float2*)u)[n * N / 2 + i];

      for (int j = 0; j < SIZE_V; j += NumPreLoad)
      {
         // Works for all parameters
         for (int index = threadIdx.x;
              index < (N / 2) * NumPreLoad && j + index * 2 / N < SIZE_V;
              index += blockDim.x)
            data[0][index] = ((float2*)v)[j * N / 2 + index];

         __syncthreads();

         for (int k = 0; k < NumPreLoad && j + k < SIZE_V; ++k)
            for (int l = 0; l < N / 2; ++l)
            {
               r[l].x += data[k][l].x;
               r[l].y += data[k][l].y;
            }

         __syncthreads();
      }

      if (n < SIZE_U)
         for (int i = 0; i < N / 2; ++i)
            ((float2*)out)[n * N / 2 + i] = r[i];
   }
}

template <int BlockDim>
__global__
void normalFloat4Shared(float * out, float *u, float *v)
{
   const int NumPreLoad = BlockDim / (N / 4) * 4; // * 2, * 3, * 4, ... if you want
   __shared__ float4 data[NumPreLoad][N / 4];

   for (int n = blockIdx.x * blockDim.x + threadIdx.x;
            n < (SIZE_U + blockDim.x - 1) / blockDim.x * blockDim.x;
            n += gridDim.x * blockDim.x)
   {
      float4 r[N / 4];

      if (n < SIZE_U)
         for (int i = 0; i < N / 4; ++i)
            r[i] = ((float4*)u)[n * N / 4 + i];

      for (int j = 0; j < SIZE_V; j += NumPreLoad)
      {
         for (int index = threadIdx.x;
              index < (N / 4) * NumPreLoad && j + index * 4 / N < SIZE_V;
              index += blockDim.x)
            data[0][index] = ((float4*)v)[j * N / 4 + index];

         __syncthreads();

         for (int k = 0; k < NumPreLoad && j + k < SIZE_V; ++k)
            for (int l = 0; l < N / 4; ++l)
            {
               r[l].x += data[k][l].x;
               r[l].y += data[k][l].y;
               r[l].z += data[k][l].z;
               r[l].w += data[k][l].w;
            }

         __syncthreads();
      }

      if (n < SIZE_U)
         for (int i = 0; i < N / 4; ++i)
            ((float4*)out)[n * N / 4 + i] = r[i];
   }
}

template <const int W>
__global__
void normalRake1Shared(float *out, float *u, float *v)
{
   const int NumPreLoad = 1;
   __shared__ float data[NumPreLoad][N];

   for (int n = inst;
            n < (SIZE_U + blockDim.x - 1) / blockDim.x * blockDim.x;
            n += gridDim.x * blockDim.x / W)
   {
      float r[N / W];

      if (n < SIZE_U)
         for (int i = 0; i < N / W; ++i)
            r[i] = u[n * N + i * W + subidx];

      for (int j = 0; j < SIZE_V; j += NumPreLoad)
      {
         for (int k = 0; k < NumPreLoad && j + k < SIZE_V; ++k)
            for (int index = threadIdx.x; index < N; index += blockDim.x)
               data[k][index] = v[(j + k) * N + index];

         __syncthreads();

         for (int k = 0; k < NumPreLoad && j + k < SIZE_V; ++k)
            for (int l = 0; l < N / W; ++l)
               r[l] += data[k][l * W + subidx];

         __syncthreads();
      }

      if (n < SIZE_U)
         for (int i = 0; i < N / W; ++i)
            out[n * N + i * W + subidx] = r[i];
   }
}

template <const int W>
__global__
void normalRake1Float2(float *out, float *u, float *v)
{
   for (int n = inst;
            n < SIZE_U;
            n += gridDim.x * blockDim.x / W)
   {
      float2 r[N / W / 2];
      for (int i = 0; i < N / W / 2; ++i)
         r[i] = ((float2*)u)[n * N / 2 + i * W + subidx];

      for (int j = 0; j < SIZE_V; ++j)
         for (int k = 0; k < N / W / 2; ++k)
         {
            float2 data = ((float2*)v)[j * N / 2 + k * W + subidx];
            r[k].x += data.x;
            r[k].y += data.y;
         }

      for (int i = 0; i < N / W / 2; ++i)
         ((float2*)out)[n * N / 2 + i * W + subidx] = r[i];
   }
}

template <const int W>
__global__
void normalRake1Float4(float *out, float *u, float *v)
{
   for (int n = inst;
            n < SIZE_U;
            n += gridDim.x * blockDim.x / W)
   {
      float4 r[N / W / 4];
      for (int i = 0; i < N / W / 4; ++i)
         r[i] = ((float4*)u)[n * N / 4 + i * W + subidx];

      for (int j = 0; j < SIZE_V; ++j)
         for (int k = 0; k < N / W / 4; ++k)
         {
            float4 data = ((float4*)v)[j * N / 4 + k * W + subidx];
            r[k].x += data.x;
            r[k].y += data.y;
            r[k].z += data.z;
            r[k].w += data.w;
         }

      for (int i = 0; i < N / W / 4; ++i)
         ((float4*)out)[n * N / 4 + i * W + subidx] = r[i];
   }
}

template <const int W>
__global__
void normalRake2Float4(float *out, float *u, float *v)
{
   for (int n = inst;
            n < SIZE_U;
            n += gridDim.x * blockDim.x / W)
   {
      float4 r[N / W / 4];
      float4 *ptr = &((float4*)u)[n * N / 4 + subidx];
      for (int i = 0; i < N / W / 4; ++i)
      {
         r[i] = *ptr;
         ptr += W;
      }

      for (int j = 0; j < SIZE_V; ++j)
      {
         ptr = &((float4*)v)[j * N / 4 + subidx];
         for (int k = 0; k < N / W / 4; ++k)
         {
            r[k].x += ptr->x;
            r[k].y += ptr->y;
            r[k].z += ptr->z;
            r[k].w += ptr->w;
            ptr += W;
         }
      }

      ptr = &((float4*)out)[n * N / 4 + subidx];
      for (int i = 0; i < N / W / 4; ++i)
      {
         *ptr = r[i];
         ptr += W;
      }
   }
}

template <const int W, int BlockDim>
__global__
void normalRake1Float4Shared(float *out, float *u, float *v)
{
   const int NumPreLoad = BlockDim / (N / 4) * 3; // * 2, * 3, * 4, ... if you want
   __shared__ float4 data[NumPreLoad][N / 4];

   for (int n = inst;
            n < (SIZE_U + blockDim.x - 1) / blockDim.x * blockDim.x;
            n += gridDim.x * blockDim.x / W)
   {
      float4 r[N / W / 4];

      if (n < SIZE_U)
         for (int i = 0; i < N / W / 4; ++i)
            r[i] = ((float4*)u)[n * N / 4 + i * W + subidx];

      for (int j = 0; j < SIZE_V; j += NumPreLoad)
      {
         for (int index = threadIdx.x;
              index < (N / 4) * NumPreLoad && j + index * 4 / N < SIZE_V;
              index += blockDim.x)
            data[0][index] = ((float4*)v)[j * N / 4 + index];

         __syncthreads();

         for (int k = 0; k < NumPreLoad && j + k < SIZE_V; ++k)
            for (int l = 0; l < N / W / 4; ++l)
            {
               float4 tmp = data[k][l * W + subidx];
               r[l].x += tmp.x;
               r[l].y += tmp.y;
               r[l].z += tmp.z;
               r[l].w += tmp.w;
            }

         __syncthreads();
      }

      if (n < SIZE_U)
         for (int i = 0; i < N / W / 4; ++i)
            ((float4*)out)[n * N / 4 + i * W + subidx] = r[i];
   }
}


// Not fixed yet fork all Rake2's
template <const int W, int BlockDim>
__global__
void normalRake2Float4Shared(float *out, float *u, float *v)
{
   const int NumPreLoad = BlockDim / (N / 4) * 3; // * 2, * 3, * 4, ... if you want
   __shared__ float4 data[NumPreLoad][N / 4];

   for (int n = inst;
            n < SIZE_U;
            n += gridDim.x * blockDim.x / W)
   {
      float4 r[N / W / 4];
      float4 *ptr = &((float4*)u)[n * N / 4 + subidx];
      for (int i = 0; i < N / W / 4; ++i)
      {
         r[i] = *ptr;
         ptr += W;
      }

      for (int j = 0; j < SIZE_V; j += NumPreLoad)
      {
         // Trick: 故意超界
         for (int index = threadIdx.x; index < (N / 4) * NumPreLoad; index += blockDim.x)
            data[0][index] = ((float4*)v)[j * N / 4 + index];

         __syncthreads();

         for (int k = 0; k < NumPreLoad && j + k < SIZE_V; ++k)
            for (int l = 0; l < N / W / 4; ++l)
            {
               float4 tmp = data[k][l * W + subidx];
               r[l].x += tmp.x;
               r[l].y += tmp.y;
               r[l].z += tmp.z;
               r[l].w += tmp.w;
            }

         __syncthreads();
      }

      ptr = &((float4*)out)[n * N / 4 + subidx];
      for (int i = 0; i < N / W / 4; ++i)
      {
         *ptr = r[i];
         ptr += W;
      }
   }
}

// =======

template <const int W, int BlockDim, int ILP>
__global__
void normalRake1Float4SharedILP(float *out, float *u, float *v)
{
   const int NumPreLoad = BlockDim / (N / 4) * 6; // * 2, * 3, * 4, ... if you want
   __shared__ float4 data[NumPreLoad][N / 4];

   int bound = ((SIZE_U + ILP - 1) / ILP + blockDim.x - 1) / blockDim.x * blockDim.x;
   for (int n = inst; n < bound; n += gridDim.x * blockDim.x / W)
   {
      float4 r[ILP][N / W / 4];

      for (int ilp = 0; ilp < ILP; ++ilp)
      {
         int index = n + ilp * bound;
         if (index >= SIZE_U) break;
         for (int i = 0; i < N / W / 4; ++i)
            r[ilp][i] = ((float4*)u)[index * N / 4 + i * W + subidx];
      }

      for (int j = 0; j < SIZE_V; j += NumPreLoad)
      {
         for (int index = threadIdx.x;
              index < (N / 4) * NumPreLoad && j + index * 4 / N < SIZE_V;
              index += blockDim.x)
            data[0][index] = ((float4*)v)[j * N / 4 + index];

         __syncthreads();

         for (int k = 0; k < NumPreLoad && j + k < SIZE_V; ++k)
            for (int l = 0; l < N / W / 4; ++l)
            {
               float4 tmp = data[k][l * W + subidx];
               for (int ilp = 0; ilp < ILP; ++ilp)
               {
                  r[ilp][l].x += tmp.x;
                  r[ilp][l].y += tmp.y;
                  r[ilp][l].z += tmp.z;
                  r[ilp][l].w += tmp.w;
               }
            }

         __syncthreads();
      }

      for (int ilp = 0; ilp < ILP; ++ilp)
      {
         int index = n + ilp * bound;
         if (index >= SIZE_U) break;
         for (int i = 0; i < N / W / 4; ++i)
            ((float4*)out)[index * N / 4 + i * W + subidx] = r[ilp][i];
      }
   }
}

void test()
{
   float *u = 0, *v = 0, *out = 0;
   float *hostU = 0, *hostV = 0, *hostOut = 0, *refOut = 0;

   CubDebug(cudaMalloc(&u, N * sizeof(float) * SIZE_U));
   CubDebug(cudaMalloc(&v, N * sizeof(float) * SIZE_V));
   CubDebug(cudaMalloc(&out, N * sizeof(float) * SIZE_U));

   hostU = new float[N * SIZE_U];
   hostV = new float[N * SIZE_V];
   hostOut = new float[N * SIZE_U];
   refOut = new float[N * SIZE_U];

   CubDebug(cudaMemset(out, 0, N * sizeof(float) * SIZE_U));

   for (int i = 0; i < N * SIZE_U; ++i)
      hostU[i] = rand() % 10;

   for (int i = 0; i < N * SIZE_V; ++i)
      hostV[i] = rand() % 10;

   for (int i = 0; i < N; ++i)
   {
      refOut[i] = 0;
      for (int j = i; j < N * SIZE_V; j += N)
         refOut[i] += hostV[j];
   }
   for (int i = N; i < N * SIZE_U; ++i)
      refOut[i] = refOut[i - N];
   for (int i = 0; i < N * SIZE_U; ++i)
      refOut[i] += hostU[i];

   cout << "Start..." << endl;

   CubDebug(cudaMemcpy(u, hostU, N * sizeof(float) * SIZE_U, cudaMemcpyDefault));
   CubDebug(cudaMemcpy(v, hostV, N * sizeof(float) * SIZE_V, cudaMemcpyDefault));

   // 416 ms, 406GB/s,
   // for (int i = 0; i < 10; ++i)
      // kernel<<<512, 512>>>(out, u, v);

   // 387 ms
   // for (int i = 0; i < 10; ++i)
      // normal<<<512, 64>>>(out, u, v);

   // 308 ms
   // for (int i = 0; i < 10; ++i)
      // normalRake2<8><<<512, 256>>>(out, u, v);

   // 286 ms
   // for (int i = 0; i < 10; ++i)
      // normalRake1<8><<<512, 128>>>(out, u, v);

   // 86.5 ms
   // for (int i = 0; i < 10; ++i)
      // normalShared<<<256, 128>>>(out, u, v);

   // 343 ms
   // for (int i = 0; i < 10; ++i)
      // normalFloat2<<<512, 64>>>(out, u, v);

   // 264 ms
   // for (int i = 0; i < 10; ++i)
      // normalFloat4<<<512, 64>>>(out, u, v);

   // cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte);

   // Nasty...
   // 81.7 ms
   // for (int i = 0; i < 10; ++i)
      // normalFloat2Shared<<<256, 128>>>(out, u, v);

   // Template: blockDim
   // 66.8 ms
   // for (int i = 0; i < 10; ++i)
      // normalFloat4Shared<128><<<256, 128>>>(out, u, v);

   // 210 ms
   // for (int i = 0; i < 10; ++i)
      // normalRake1Shared<8><<<256, 128>>>(out, u, v);

   // 205 ms
   // for (int i = 0; i < 10; ++i)
      // normalRake1Float2<8><<<256, 128>>>(out, u, v);

   // 212 ms
   // for (int i = 0; i < 10; ++i)
      // normalRake1Float4<4><<<256, 256>>>(out, u, v);

   // 192 ms
   // for (int i = 0; i < 10; ++i)
   //    normalRake2Float4<4><<<256, 256>>>(out, u, v);

   // cudaDeviceSynchronize();

   // 63 ms
   // for (int i = 0; i < 10; ++i)
      // normalRake1Float4Shared<4, 512><<<256, 512>>>(out, u, v);

   // 65.2 ms
   // for (int i = 0; i < 10; ++i)
      // normalRake2Float4Shared<4, 512><<<256, 512>>>(out, u, v);

   // ====== ILP

   // 37.3 ms
   // for (int i = 0; i < 10; ++i)
      // normalRake1Float4SharedILP<4, 128, 4><<<256, 128>>>(out, u, v);

   CubDebug(cudaMemcpy(hostOut, out, N * sizeof(float) * SIZE_U, cudaMemcpyDefault));

   for (int i = 0; i < N * SIZE_U; ++i)
      if (hostOut[i] != refOut[i])
      {
         cout << "error: idx " << i << " (" << i / N << ", " << i % N << ") , out " << hostOut[i] << ", ref " << refOut[i] << endl;
         break;
      }

#if 0
   for (int i = 0; i < 10; ++i)
      cout << hostU[i] << " ";
   cout << endl;
   for (int i = 0; i < 10; ++i)
      cout << hostV[i] << " ";
   cout << endl;
   for (int i = 0; i < 10; ++i)
      cout << refOut[i] << " ";
   cout << endl;
   for (int i = 0; i < 10; ++i)
      cout << hostOut[i] << " ";
   cout << endl;
#endif

   CubDebug(cudaFree(u));
   CubDebug(cudaFree(v));
   CubDebug(cudaFree(out));
   delete[] hostU;
   delete[] hostV;
   delete[] hostOut;
   delete[] refOut;

   cout << "Done" << endl;
}


#if 0
void testPitch()
{
   float *u = 0, *v = 0, *out = 0;
   float *hostU = 0, *hostV = 0, *hostOut = 0, *refOut = 0;

   hostU = new float[N * SIZE_U];
   hostV = new float[N * SIZE_V];
   hostOut = new float[N * SIZE_U];
   refOut = new float[N * SIZE_U];

   size_t pitch = 0, pitch1 = 0, pitch2 = 0;

   CubDebug(cudaMallocPitch(&u, &pitch, N * sizeof(float), SIZE_U));
   CubDebug(cudaMallocPitch(&v, &pitch1, N * sizeof(float), SIZE_V));
   CubDebug(cudaMallocPitch(&out, &pitch2, N * sizeof(float), SIZE_U));

   assert(pitch == pitch1);
   assert(pitch == pitch2);

   cout << "Pitch: " << pitch << endl;

   CubDebug(cudaMemset2D(out, pitch, 0, N * sizeof(float), SIZE_U));

   for (int i = 0; i < N * SIZE_U; ++i)
      hostU[i] = rand() % 10;

   for (int i = 0; i < N * SIZE_V; ++i)
      hostV[i] = rand() % 10;

   for (int i = 0; i < N; ++i)
   {
      refOut[i] = 0;
      for (int j = i; j < N * SIZE_V; j += N)
         refOut[i] += hostV[j];
   }
   for (int i = N; i < N * SIZE_U; ++i)
      refOut[i] = refOut[i - N];
   for (int i = 0; i < N * SIZE_U; ++i)
      refOut[i] += hostU[i];

   cout << "Start..." << endl;

   CubDebug(cudaMemcpy2D(u, pitch, hostU, N * sizeof(float), N * sizeof(float), SIZE_U, cudaMemcpyHostToDevice));
   CubDebug(cudaMemcpy2D(v, pitch, hostV, N * sizeof(float), N * sizeof(float), SIZE_V, cudaMemcpyHostToDevice));

   // Note: Pass in pitch in words, not in bytes

   // for (int i = 0; i < 10; ++i)
      // normalPitch<<<512, 64>>>(out, u, v, pitch / sizeof(float));

   // 341 ms
   // for (int i = 0; i < 10; ++i)
      normalRakePitch<8><<<512, 128>>>(out, u, v, pitch / sizeof(float));

   CubDebug(cudaMemcpy2D(hostOut, N * sizeof(float), out, pitch, N * sizeof(float), SIZE_U, cudaMemcpyDeviceToHost));

   for (int i = 0; i < N * SIZE_U; ++i)
      if (hostOut[i] != refOut[i])
      {
         cout << "error: idx " << i << ", out " << hostOut[i] << ", ref " << refOut[i] << endl;
         break;
      }

   CubDebug(cudaFree(u));
   CubDebug(cudaFree(v));
   CubDebug(cudaFree(out));
   delete[] hostU;
   delete[] hostV;
   delete[] hostOut;
   delete[] refOut;

   cout << "Done" << endl;
}
#endif

int main()
{
   test();
}