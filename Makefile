CC      = g++
CPPFLAG = -O3
LIBS    = -lntl -lboost_serialization

# Modified version of cub to solve too less shared memory problem
NVARCH  = -arch=sm_35 -use_fast_math -I../cub-1.3.2 #-Xptxas=-v

OBJ   = gsieve_main.o gsieve.o common.o


all: $(OBJ)
	nvcc -o g $^ $(LIBS) $(CPPFLAG) $(NVARCH)

sampler: sampler.o common.o
	$(CC) -o $@ $^ $(LIBS) $(CPPFLAG)

%.o: %.cpp
	$(CC) -c $^ $(CPPFLAG)

%.o: %.cu
	nvcc -c $^ $(CPPFLAG) $(NVARCH)

# all: gsieve_main.cc gsieve.cu sampler.cc common.cc
# 	nvcc gsieve_main.cc gsieve.cu sampler.cc common.cc -arch=sm_35 -lntl -o g

# t: test.cu
# 	nvcc test.cu -arch=sm_35 -lntl -o t

s: gsieve.cu
	nvcc gsieve.cu $(NVARCH) -cubin -o t.cubin -Xptxas=-v
	cuobjdump -sass t.cubin > t.s

clean:
	rm *.o g