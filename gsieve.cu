#include "gsieve.h"
#include <cassert>
#include <fstream>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#define CUB_STDERR

#include <cub/cub.cuh>

using namespace cub;

#undef CubDebug
#define CubDebug CubDebugExit

const int N = 80;
const int RakeWidth = 2;

const int ILP = 4;
const int BlockDim = 128;

const int NumSamples = 16384;

// Why not "typedef float Point[N];" Ans: cub template needs this in order to work
struct Point
{
   float data[N];

   __device__ __host__ float& operator[](size_t idx) { return data[idx]; }
   __device__ __host__ const float& operator[](size_t idx) const { return data[idx]; }

   template<class Archive>
   void serialize(Archive & ar, const unsigned int version)
   {
      ar & data;
   }
};

__device__ __managed__ Point *samples;
__device__ __managed__ float *samples_norm;

__device__ __managed__ Point *gstack;
__device__ __managed__ float *gstack_norm;

__device__ __managed__ Point *glist;
__device__ __managed__ float *glist_norm;

__device__ __managed__ Point *samples2;
__device__ __managed__ float *samples2_norm;

__device__ __managed__ Point *glist2;
__device__ __managed__ float *glist2_norm;

int list_size = 0;
int stack_size = 0;

__device__ __managed__ int d_num_selected = 0;

__device__ __managed__ ItemOffsetPair<float, int> argmin;

#define idx    (blockIdx.x * blockDim.x + threadIdx.x)
#define inst   (idx / RakeWidth)
#define subidx (idx % RakeWidth)

struct NonReduced
{
   __host__ __device__ __forceinline__
   bool operator()(float n) const
   {
      return n > 10;
   }
};

struct NonCollision
{
   __host__ __device__ __forceinline__
   bool operator()(float n) const
   {
      return n > 1;
   }
};

// Global functions
void PrintList(string name, Point *list, float *norm, size_t n)
{
   cout << name << ":" << endl;
   for (int i = 0; i < n; ++i)
   {
      cout << i << " (" << int(sqrt(norm[i])) << ") = [";
      for (int j = 0; j < ::min(16, N); ++ j)
         cout << list[i][j] << " ";
      cout << "...]" << endl;
   }
}

void* d_temp = 0;
size_t temp_size = 0;

// Warppers for CUB functions
template <typename InputIterator, typename FlagIterator, typename OutputIterator>
void Partition(InputIterator d_in, FlagIterator d_flags, OutputIterator d_out, int num_items)
{
   size_t bytes;
   CubDebug(DevicePartition::Flagged(0, bytes, d_in, d_flags, d_out, &d_num_selected, num_items));
   if (bytes > temp_size)
   {
      CubDebug(cudaFree(d_temp));
      CubDebug(cudaMalloc(&d_temp, bytes));
      temp_size = bytes;
   }
   CubDebug(DevicePartition::Flagged(d_temp, bytes, d_in, d_flags, d_out, &d_num_selected, num_items));
   CubDebug(cudaDeviceSynchronize());
}

template <typename InputIterator, typename FlagIterator, typename OutputIterator>
void Select(InputIterator d_in, FlagIterator d_flags, OutputIterator d_out, int num_items)
{
   size_t bytes;
   CubDebug(DeviceSelect::Flagged(0, bytes, d_in, d_flags, d_out, &d_num_selected, num_items));
   if (bytes > temp_size)
   {
      CubDebug(cudaFree(d_temp));
      CubDebug(cudaMalloc(&d_temp, bytes));
      temp_size = bytes;
   }
   CubDebug(DeviceSelect::Flagged(d_temp, bytes, d_in, d_flags, d_out, &d_num_selected, num_items));
   CubDebug(cudaDeviceSynchronize());
}

template <typename InputIterator, typename OutputIterator, typename SelectOp>
void SelectIf(InputIterator d_in, OutputIterator d_out, int num_items, SelectOp select_op)
{
   size_t bytes;
   CubDebug(DeviceSelect::If(0, bytes, d_in, d_out, &d_num_selected, num_items, select_op));
   if (bytes > temp_size)
   {
      CubDebug(cudaFree(d_temp));
      CubDebug(cudaMalloc(&d_temp, bytes));
      temp_size = bytes;
   }
   CubDebug(DeviceSelect::If(d_temp, bytes, d_in, d_out, &d_num_selected, num_items, select_op));
   CubDebug(cudaDeviceSynchronize());
}

template<typename InputIterator, typename OutputIterator>
void ArgMin(InputIterator d_in, OutputIterator d_out, int num_items)
{
   size_t bytes;
   CubDebug(cub::DeviceReduce::ArgMin(0, bytes, d_in, d_out, num_items));
   if (bytes > temp_size)
   {
      CubDebug(cudaFree(d_temp));
      CubDebug(cudaMalloc(&d_temp, bytes));
      temp_size = bytes;
   }
   CubDebug(cub::DeviceReduce::ArgMin(d_temp, bytes, d_in, d_out, num_items));
   CubDebug(cudaDeviceSynchronize());
}

void PushToStack(Point* data, size_t size)
{
   CubDebug(cudaMemcpy(gstack + stack_size, data, sizeof(Point) * size, cudaMemcpyDefault));
   stack_size += size;
}

mat_RR B_inv;

void SanityCheck(string name, Point *list, size_t size)
{
   cout << "Sanity check... " << name << endl;

   mat_RR vs, vs2;
   vs.SetDims(size, N);
   vs2.SetDims(size, N);

   for (int i = 0; i < size; ++i)
      for (int j = 0; j < N; ++j)
         vs[i][j] = list[i][j];

      cout << "===" << endl;

   vs2 = vs * B_inv;
   cout << "===" << endl;

   bool ok = true;
   int n = 0;
   for (int i = 0; i < size; ++i)
      for (int j = 0; j < N; ++j)
         if (abs(conv<float>(vs2[i][j]) - conv<int>(round(vs2[i][j]))) > 0.05)
         {
            cout << name << " Error: " << i << endl;
            ok = false;
            n = i;
            break;
         }
   if (!ok)
      cout << vs[n] <<  endl << "--------" << endl << vs2[n] << endl;
}

//////////////////

void GSieve::Summary()
{
   cout << "\t\033[1;32mList = " << list_size << ", Stack = " << stack_size
        /* << ", List1 = " << list1Size << ", List2 = " << list2Size */ << "\033[0m" << endl;
}

void GSieve::InitGPU(const mat_ZZ &B)
{
   B_inv = inv(conv<mat_RR>(B));

   CubDebug(cudaSetDevice(0));

   CubDebug(cudaMallocManaged(&samples, sizeof(Point) * NumSamples * 20));
   CubDebug(cudaMallocManaged(&samples_norm, sizeof(float) * NumSamples * 20));

   CubDebug(cudaMallocManaged(&gstack, sizeof(Point) * 2000000));
   CubDebug(cudaMallocManaged(&gstack_norm, sizeof(float) * 2000000));

   CubDebug(cudaMallocManaged(&glist, sizeof(Point) * 2000000));
   CubDebug(cudaMallocManaged(&glist_norm, sizeof(float) * 2000000));

   CubDebug(cudaMallocManaged(&samples2, sizeof(Point) * NumSamples * 20));
   CubDebug(cudaMallocManaged(&samples2_norm, sizeof(float) * NumSamples * 20));

   CubDebug(cudaMallocManaged(&glist2, sizeof(Point) * 2000000));
   CubDebug(cudaMallocManaged(&glist2_norm, sizeof(float) * 2000000));

   // Initialize list and stack
   cout << "Initializing global list and stack..." << endl;

   // Initialize list
   list_size = list_.size();
   list<ListPoint *>::iterator it = list_.begin();
   for (int i = 0; it != list_.end(); ++it, ++i)
   {
      for (int j = 0; j < N; ++j)
         glist[i][j] = conv<float>((*it)->v[j]);
      glist_norm[i] = conv<float>((*it)->norm);
   }

   // Initialize stack
   int cnt = 0;
   while (!queue_.empty())
   {
      ListPoint *p = queue_.front();
      queue_.pop();

      for (int j = 0; j < N; ++j)
         gstack[cnt][j] = conv<float>(p->v[j]);
      gstack_norm[cnt] = conv<float>(p->norm);
      DeleteListPoint(p);
      ++cnt;
   }
   stack_size = cnt;

   Summary();
}

template <int step>
__global__
void ref(Point * __restrict__ wlist, float * __restrict__ wlist_norm, size_t wlist_size, const Point * __restrict__ vlist, const float * __restrict__ vlist_norm, size_t vlist_size)
{
   for (int n = blockIdx.x * blockDim.x + threadIdx.x; n < wlist_size; n += gridDim.x * blockDim.x)
   {
      float r[N];
      for (int i = 0; i < N; ++i)
         r[i] = wlist[n][i];

      for (int j = 0; j < vlist_size; ++j)
      {
         float v[N], v_norm = vlist_norm[j];
         float dot = 0;

         if (!NonReduced()(v_norm)) continue;

         for (int k = 0; k < N; ++k)
         {
            v[k] = vlist[j][k];
            dot += r[k] * v[k];
         }

         float q = round(dot / v_norm);

         if (step == 1 && j == n)
            q = 0;

         for (int k = 0; k < N; ++k)
            r[k] -= q * v[k];

         if (q != 0)
            wlist_norm[n] = 5;
      }

      for (int i = 0; i < N; ++i)
         wlist[n][i] = r[i];
   }
}


template <int step>
__global__
void reduce(Point * __restrict__ wlist, float * __restrict__ wlist_norm, size_t wlist_size, const Point * __restrict__ vlist, const float * __restrict__ vlist_norm, size_t vlist_size)
{
   const int NumPreLoad = BlockDim / (N / 4) * 8; // * 2, * 3, * 4, ... if you want
   __shared__ float4 data[NumPreLoad][N / 4];

   int bound = ((wlist_size + ILP - 1) / ILP + blockDim.x - 1) / blockDim.x * blockDim.x;
   for (int n = inst; n < bound; n += gridDim.x * blockDim.x / RakeWidth)
   {
      float4 r[ILP][N / RakeWidth / 4];

      for (int ilp = 0; ilp < ILP; ++ilp)
      {
         int index = n + ilp * bound;
         if (index >= wlist_size) break;
         for (int i = 0; i < N / RakeWidth / 4; ++i)
            r[ilp][i] = ((float4*)wlist)[index * N / 4 + i * RakeWidth + subidx];
      }

      for (int j = 0; j < vlist_size; j += NumPreLoad)
      {
         for (int index = threadIdx.x;
              index < (N / 4) * NumPreLoad && j + index * 4 / N < vlist_size;
              index += blockDim.x)
            data[0][index] = ((float4*)vlist)[j * N / 4 + index];

         __syncthreads();

         for (int k = 0; k < NumPreLoad && j + k < vlist_size; ++k)
         {
            float v_norm = vlist_norm[j + k];

            if (!NonReduced()(v_norm)) continue;

            float dot[ILP] = {0};
            for (int l = 0; l < N / RakeWidth / 4; ++l)
            {
               float4 tmp = data[k][l * RakeWidth + subidx];

               for (int ilp = 0; ilp < ILP; ++ilp)
                  dot[ilp] += r[ilp][l].x * tmp.x + r[ilp][l].y * tmp.y + r[ilp][l].z * tmp.z + r[ilp][l].w * tmp.w;
            }

            for (int l = 1; l < RakeWidth; l *= 2)
               for (int ilp = 0; ilp < ILP; ++ilp)
                  dot[ilp] += __shfl_xor(dot[ilp], l);

            for (int ilp = 0; ilp < ILP; ++ilp)
            {
               float q = round(dot[ilp] / v_norm);

               if (step == 1 && j + k == n + ilp * bound)
                  q = 0;

               for (int l = 0; l < N / RakeWidth / 4; ++l)
               {
                  float4 tmp = data[k][l * RakeWidth + subidx];

                  r[ilp][l].x -= q * tmp.x;
                  r[ilp][l].y -= q * tmp.y;
                  r[ilp][l].z -= q * tmp.z;
                  r[ilp][l].w -= q * tmp.w;
               }

               if (q != 0)
                  wlist_norm[n + ilp * bound] = 5;
            }
         }

         __syncthreads();
      }

      for (int ilp = 0; ilp < ILP; ++ilp)
      {
         int index = n + ilp * bound;
         if (index >= wlist_size) break;
         for (int i = 0; i < N / RakeWidth / 4; ++i)
            ((float4*)wlist)[index * N / 4 + i * RakeWidth + subidx] = r[ilp][i];
      }
   }
}

void GSieve::Start()
{
   float shortest_vector[N] = {0};

   for (;_iterations < 100000 && _collisions < 0.1 * _max_list_size + 200; ++_iterations)
   {
      // if (_iterations == 20)
      //    Save("data20");

      cout << "====== Iteration " << _iterations << " ======" << endl;
      GenerateSamples();

      reduce<0><<<128, BlockDim>>>(samples, samples_norm, NumSamples, glist, glist_norm, list_size);
      CubDebug(cudaDeviceSynchronize());

      CubDebug(cudaMemcpy(samples2, samples, sizeof(Point) * NumSamples, cudaMemcpyDefault));
      CubDebug(cudaMemcpy(samples2_norm, samples_norm, sizeof(float) * NumSamples, cudaMemcpyDefault));

      reduce<1><<<128, BlockDim>>>(samples2, samples2_norm, NumSamples, samples, samples_norm, NumSamples);
      CubDebug(cudaDeviceSynchronize());

      reduce<0><<<128, BlockDim>>>(glist, glist_norm, list_size, samples2, samples2_norm, NumSamples);
      CubDebug(cudaDeviceSynchronize());

      Summary();

      // Note: (a) reduced (<1) includes collision (==0)
      //       (b) Partition is not in-place
      // 1. Partition list (non-reduced / reduced) to list2, copy non-reduced from list2 to list, put reduced to stack, select non-reduced norm
      // 2. Partition samples2 (non-reduced / reduced) to list, put reduced to stack, select non-reduced norm
      // 3. Calculate stack norm
      // 4. Select not collision from stack, accumulate collision number

      // 1. Partition list (non-reduced / reduced) to list2, copy non-reduced from list2 to list, put reduced to stack, select non-reduced norm
      TransformInputIterator<bool, NonReduced, float*> itr1(glist_norm, NonReduced());
      Partition(glist, itr1, glist2, list_size);
      CubDebug(cudaMemcpy(glist, glist2, sizeof(Point) * d_num_selected, cudaMemcpyDefault));
      CubDebug(cudaMemcpy(gstack + stack_size, glist2 + d_num_selected, sizeof(Point) * (list_size - d_num_selected), cudaMemcpyDefault));
      stack_size += list_size - d_num_selected;

      SelectIf(glist_norm, glist_norm, list_size, NonReduced());
      list_size = d_num_selected;

      Summary();

      // 2. Partition samples2 (non-reduced / reduced) to list, put reduced to stack, select non-reduced norm
      TransformInputIterator<bool, NonReduced, float*> itr2(samples2_norm, NonReduced());
      Partition(samples2, itr2, glist + list_size, NumSamples);
      CubDebug(cudaMemcpy(gstack + stack_size, glist + list_size + d_num_selected, sizeof(Point) * (NumSamples - d_num_selected), cudaMemcpyDefault));
      stack_size += NumSamples - d_num_selected;

      SelectIf(samples2_norm, glist_norm + list_size, NumSamples, NonReduced());
      list_size += d_num_selected;

      Summary();

      // 3. Calculate stack norm (this is doing redundant work)
      for (int i = 0; i < stack_size; ++i)
      {
         float norm = 0;
         for (int j = 0; j < N; ++j)
            norm += gstack[i][j] * gstack[i][j];
         gstack_norm[i] = norm;
      }

      // PrintList("stack_before", gstack, gstack_norm, stack_size);

      // =========== Why the heck doesn't this work!!?
      // the documetation says these can be in-place, but it has different results every time
      // fxck, i spent an entire week debugging this
      // .... but why does SelectIf in step 1 work...???
      // 4. Select non-collision from stack, accumulate collision number
      // TransformInputIterator<bool, NonCollision, float*> itr3(gstack_norm, NonCollision());
      // Select(gstack, itr3, gstack, stack_size);
      // SelectIf(gstack_norm, gstack_norm, stack_size, NonCollision());
      // collisions += stack_size - d_num_selected;
      // stack_size = d_num_selected;
      TransformInputIterator<bool, NonCollision, float*> itr3(gstack_norm, NonCollision());
      Select(gstack, itr3, glist2, stack_size);
      SelectIf(gstack_norm, glist2_norm, stack_size, NonCollision());
      _collisions += stack_size - d_num_selected;
      stack_size = d_num_selected;
      CubDebug(cudaMemcpy(gstack, glist2, sizeof(Point) * stack_size, cudaMemcpyDefault));
      CubDebug(cudaMemcpy(gstack_norm, glist2_norm, sizeof(float) * stack_size, cudaMemcpyDefault));

      Summary();

      cout << "Collisions: " << _collisions << endl;

      ::ArgMin(gstack_norm, &argmin, stack_size);
      if (argmin.value < _best_sqr_norm)
      {
         _best_sqr_norm = argmin.value;
         for (int i = 0; i < N; ++i)
            shortest_vector[i] = gstack[argmin.offset][i];
      }

      ::ArgMin(glist_norm, &argmin, list_size);
      if (argmin.value < _best_sqr_norm)
      {
         _best_sqr_norm = argmin.value;
         for (int i = 0; i < N; ++i)
            shortest_vector[i] = glist[argmin.offset][i];
      }

      cout << "Min Norm = " << int(_best_sqr_norm) << endl;
      _max_list_size = ::max(list_size, _max_list_size);

      cout << '[';
      for (int i = 0; i < N; ++i)
         cout << shortest_vector[i] << ' ';
      cout << ']' << endl;
   }
}

void GSieve::GenerateSamples()
{
   int copy_size = ::min(NumSamples, stack_size);
   CubDebug(cudaMemcpy(samples, gstack + stack_size - copy_size, sizeof(Point) * copy_size, cudaMemcpyDefault));
   CubDebug(cudaMemcpy(samples_norm, gstack_norm + stack_size - copy_size, sizeof(float) * copy_size, cudaMemcpyDefault));

   stack_size -= copy_size;

   cout << "Generate " << NumSamples - copy_size << " new samples..." << endl;
   for (int i = copy_size; i < NumSamples; ++i)
   {
      for (int j = 0; j < N; ++j)
         cin >> samples[i][j];
      cin >> samples_norm[i];
   }

   Summary();
}

void GSieve::StopGPU()
{
   CubDebug(cudaFree(samples));
   CubDebug(cudaFree(samples_norm));

   CubDebug(cudaFree(gstack));
   CubDebug(cudaFree(gstack_norm));

   CubDebug(cudaFree(glist));
   CubDebug(cudaFree(glist_norm));

   CubDebug(cudaFree(samples2));
   CubDebug(cudaFree(samples2_norm));

   CubDebug(cudaFree(glist2));
   CubDebug(cudaFree(glist2_norm));

   CubDebug(cudaDeviceReset());
}

void GSieve::Save(string filename)
{
   ofstream fout(filename.c_str());
   boost::archive::text_oarchive oa(fout);

   oa & _iterations & _collisions & _max_list_size & _best_sqr_norm;
   oa & stack_size & list_size;

   oa & boost::serialization::make_array(gstack, stack_size);
   oa & boost::serialization::make_array(gstack_norm, stack_size);

   oa & boost::serialization::make_array(glist, list_size);
   oa & boost::serialization::make_array(glist_norm, list_size);

   cout << "Data saved to " << filename << endl;
}

void GSieve::Read(string filename)
{
   ifstream fin(filename.c_str());
   boost::archive::text_iarchive ia(fin);

   ia & _iterations & _collisions & _max_list_size & _best_sqr_norm;
   ia & stack_size & list_size;

   ia & boost::serialization::make_array(gstack, stack_size);
   ia & boost::serialization::make_array(gstack_norm, stack_size);

   ia & boost::serialization::make_array(glist, list_size);
   ia & boost::serialization::make_array(glist_norm, list_size);

   cout << "Data read from " << filename << endl;
   cout << "Remeber to use new seed for rand(). Otherwise you will get a lot of collisions." << endl;

   Summary();
}

// Cleans up used memory
void GSieve::CleanUp()
{
   list<ListPoint *>::iterator lp_it;
   for (lp_it = list_.begin(); lp_it != list_.end(); ++lp_it)
      DeleteListPoint(*lp_it);
   list_.clear();
   while (!queue_.empty())
   {
      DeleteListPoint(queue_.front());
      queue_.pop();
   }
}

// Initializes the reducer.
void GSieve::Init(const mat_ZZ &B)
{
   assert(B.NumRows() == N);

   n_ = B.NumRows();
   m_ = B.NumCols();

   _best_sqr_norm = to_long(B[0] * B[0]);
   ListPoint *p;
   long current_norm;
   for (int i = 0; i < n_; ++i)
   {
      p = NewListPoint(m_);
      VecZZToListPoint(B[i], p);
      current_norm = UpdateList(p);
      if (current_norm < _best_sqr_norm)
         _best_sqr_norm = current_norm;
   }

   _iterations = 0;
   _collisions = 0;
   _max_list_size = 0;
   _best_sqr_norm = 1e30;
}

void GSieve::SetGoalSqrNorm(long norm)
{
   goal_sqr_norm_ = norm;
}

void GSieve::SetVerbose(bool verbose)
{
   verbose_ = verbose;
}

// Reduces recuirsively the point with all the points with smaller norm
// Adds the point to the list if we don't have colission
// and puts to the queue all the points with bigger norm
// that can be reduced with it.
int GSieve::UpdateList(ListPoint *p)
{
   list<ListPoint *>::iterator lp_it, tmp_lp_it;
   ListPoint *lp;
   bool needs_reduction = true;
   // Reduce the new lattice point
   // with the vectors with smaller norm
   while (needs_reduction)
   {
      needs_reduction = false;
      for (lp_it = list_.begin(); lp_it != list_.end(); ++lp_it)
      {
         lp = *lp_it;
         if (lp->norm > p->norm)
            break;
         //If there is one reduction the vector should re-pass the list
         if (Reduce(p, lp))
            needs_reduction = true;
      }
   }
   // We got a collision
   if (p->norm == 0)
   {
      DeleteListPoint(p);
      return 0;
   }
   // lp_it shows to the first point with bigger norm
   // this is where we will insert the new point
   list_.insert(lp_it, p);
   // Let's reduce now the vectors with bigger norm
   while (lp_it != list_.end())
   {
      tmp_lp_it = lp_it;
      lp = *lp_it;
      ++lp_it;
      if (Reduce(lp, p))
      {
         list_.erase(tmp_lp_it);
         queue_.push(lp);
      }
   }
   return p->norm;
}
