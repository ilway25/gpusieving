#ifndef __GSIEVE__
#define __GSIEVE__

#include <iostream>
#include <list>
#include <queue>

#include <NTL/ZZ.h>
#include <NTL/vec_ZZ.h>
#include <NTL/mat_ZZ.h>
#include <NTL/LLL.h>
#include "common.h"
#include "sampler.h"

NTL_CLIENT

class GSieve {
  public:
    ~GSieve() {
      CleanUp();
    }
    void Init(const mat_ZZ& B);
    void SetGoalSqrNorm(long norm);
    void SetVerbose(bool verbose);
    void CleanUp();
    // bool Start();

    void InitGPU(const mat_ZZ &B);
    void Start();
    void StopGPU();

    void GenerateSamples();

    void Summary();

    void Save(string);
    void Read(string);

  private:
    int UpdateList(ListPoint* p);
    KleinSampler* sampler_;
    long n_;
    long m_;
    int64 goal_sqr_norm_;
    list<ListPoint*> list_;
    queue<ListPoint*> queue_;
    // Statistics
    bool verbose_;

    // Status

    float _best_sqr_norm;
    int   _max_list_size;
    int   _iterations;
    int   _collisions;
};

#endif
